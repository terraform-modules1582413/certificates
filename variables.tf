variable "crts" {
  type = list(object({
    name      = string
    namespace = string
    crt       = string
    key       = string
  }))
}