resource "kubernetes_manifest" "certificate" {
  for_each = { for crt in var.crts : crt.name => crt }
  manifest = yamldecode(templatefile("${path.module}/crt.yaml", {
    name      = each.value.name
    namespace = each.value.namespace
    crt       = each.value.crt
    key       = each.value.key
  }))
}